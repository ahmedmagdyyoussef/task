Hello..

These are an important information you need to know them before execution:

1- This project added to GITLab and in public repository. 
2- This project is a selenium Java with MAVEN and SHAFT framwork (latest automation framwork)
3- I used an integrated report fram work that generated inside the project in the extent report folder. 
4- If you need to change the browser only you need to configure it from the properties file that exist inside the project. 
5- You can run it on different browser 
6- If you need to make parrallel execution you can configure it from the project properties. 
7- I created Json file and i added inside it the required Testdata and it should be per Test case, each one 
has an item in the JSON file with test case name. 
8- I use hard assert not soft to stop once any issue happened and log it in the report to know the root cause of this issue
9- Related to API , i used a mockup online APIS that worked with as i tried your API mockup that sent but it was not working with on there portal.
10- I used GITLab as CI and versioning for my code.
11- Code written in Java with Intelli4j IDE.

