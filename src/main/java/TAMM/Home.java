package TAMM;

import com.shaft.gui.browser.BrowserActions;
import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Home {
    //Variables
    WebDriver driver;

    public static By getSearchBox() {
        return SearchBox;
    }

    //Locater if i made it public you can access them
    private static  By SearchBox = By.id("content");
    private static  By StartAndManageCategory = By.xpath("//*[@id=\"content\"]/div/div[1]/div/div[2]/div/div/div/div/div[3]/a/div/div[1]");




  /*
    public StartAndManage SearchForText(String KeyWord)
    {
        ElementActions.type(driver,getTextBox(),KeyWord);
        ElementActions.keyPress(driver,getTextBox(), Keys.ENTER);
        return new StartAndManage(driver);
    }*/


    public Home CheckSearchBox(String URL){

        return this;
    }
    public Home Navigate(String URL){
        BrowserActions.navigateToURL(driver,URL);
        return this;
    }
    public void ClickOnSpecificCategory(){
        ElementActions.click(driver,StartAndManageCategory);
    }
    public Home(WebDriver DriverFromCalledClass){
        this.driver=DriverFromCalledClass;
    }
}
