package TAMM;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class StartAndManage {
    WebDriver driver;
    private static String Question1Text;
    private static String Question1Option1;
    private static String Question1Option2;
    private static String Question1Option3;
    private static String Question1Option4;
    private static String Question1Option5;
    private static String Question1Option6;
    private static String Question2Text;
    private static String Question2Option1;
    private static String Question2Option2;
    private static String Question2Option3;


    private static String PreviousAnswer;
    private static By Question1=By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div/h3");
    private static By SearchResultCounter = By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div/div/div/div/div/div[2]/div/div[1]/p");
    private static By StepByStep = By.xpath("//*[@id=\"app\"]/div/div/div[2]/div/div/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/a/div[2]/div[1]/div[2]");
    private static By Question1Option1Locator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div[1]/div/div/div/label[1]/div[2]/h5");
    private static By Question1Option2Locator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div[1]/div/div/div/label[2]/div[2]/h5");
    private static By Question1Option3Locator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div[1]/div/div/div/label[3]/div[2]/h5");
    private static By Question1Option4Locator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div[1]/div/div/div/label[4]/div[2]/h5");
    private static By Question1Option5Locator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div[1]/div/div/div/label[5]/div[2]/h5");
    private static By Question1Option6Locator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div[1]/div/div/div/label[6]/div[2]/h5");
    private static By NextButton= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div[2]/div");
    private static By NextButtonQuestion2= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[1]/div[2]/div/button[2]");
    private static By PreviousAnswerLocatorQuestion2= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[2]/div[2]/div[2]/div/div[2]");
    private static By PreviousAnswerLocator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[2]/div[2]/div[2]/div/div[2]");
    private static By RadioButton1= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div[1]/div/div/div/label[1]");
    private static By RadioButton2= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[1]/div[1]/div/div/div/label[3]");
    private static By RadioButton3= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[1]/div[1]/div/div/div/label[1]");
    private  static By AcceptCookies= By.xpath("//*[@id=\"app\"]/div/div/div[5]/div[1]/div[2]/button");
    private static By Question2=By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[1]/h3");
    private static By Question2Option1Locator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[1]/div[1]/div/div/div/label[1]/div[2]/h5");
    private static By Question2Option2Locator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[1]/div[1]/div/div/div/label[2]/div[2]/h5");
    private static By Question2Option3Locator= By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[1]/div[1]/div/div/div/label[3]/div[2]/h5");

//*[@id="app"]/div/div/div[3]/div/div[1]/div/div[1]/div/div[1]/div[2]/div/button[2]

    public StartAndManage(WebDriver DriverFromCalledClass){
        this.driver=DriverFromCalledClass;
    }

    public static String getQuestion1Text() {
        return Question1Text;
    }
    public static String getQuestion2Text() {
        return Question2Text;
    }
    public static By getPreviousAnswerLocator() {
        return PreviousAnswerLocator;
    }
    public static By getSearchResultCounter() {
        return SearchResultCounter;
    }
    public static By getPreviousAnswerLocatorQuestion2() {
        return PreviousAnswerLocatorQuestion2;
    }
    public static String getQuestion1Option1() {
        return Question1Option1;
    }

    public static String getQuestion1Option2() {
        return Question1Option2;
    }

    public static String getQuestion1Option3() {
        return Question1Option3;
    }

    public static String getQuestion1Option4() {
        return Question1Option4;
    }

    public static String getQuestion1Option5() {
        return Question1Option5;
    }

    public static String getQuestion1Option6() {
        return Question1Option6;
    }
//Second Question options strings
    public static String getQuestion2Option1() {
        return Question2Option1;
    }

    public static String getQuestion2Option2() {
        return Question2Option2;
    }

    public static String getQuestion2Option3() {
        return Question2Option3;
    }



    public static By getQuestion1() {
        return Question1;
    }
    public static By getQuestion2() {
        return Question2;
    }

    public static String getPreviousAnswer() {
        return PreviousAnswer;
    }

    public StartAndManage CheckQuestion1Text(){
        Question1Text=ElementActions.getText(driver,Question1);
        return this;
    }
    public StartAndManage CheckQuestion2Text(){
        Question2Text=ElementActions.getText(driver,Question2);
        return this;
    }
    public StartAndManage StepByStepClick(){
       ElementActions.click(driver,StepByStep);
        return this;
    }
    public StartAndManage CheckQuestion1Option1Text(){
        Question1Option1=ElementActions.getText(driver,Question1Option1Locator);
        return this;
    }
    public StartAndManage CheckQuestion1Option2Text(){
        Question1Option2=ElementActions.getText(driver,Question1Option2Locator);
        return this;
    }
    public StartAndManage CheckQuestion1Option3Text(){
        Question1Option3=ElementActions.getText(driver,Question1Option3Locator);
        return this;
    }
    public StartAndManage CheckQuestion1Option4Text(){
        Question1Option4=ElementActions.getText(driver,Question1Option4Locator);
        return this;
    }
    public StartAndManage CheckQuestion1Option5Text(){
        Question1Option5=ElementActions.getText(driver,Question1Option5Locator);
        return this;
    }
    public StartAndManage CheckQuestion1Option6Text(){
        Question1Option6=ElementActions.getText(driver,Question1Option6Locator);
        return this;
    }

    //Get Question 2 options text
    public StartAndManage CheckQuestion2Option1Text(){
        Question2Option1=ElementActions.getText(driver,Question2Option1Locator);
        return this;
    }
    public StartAndManage CheckQuestion2Option2Text(){
        Question2Option2=ElementActions.getText(driver,Question2Option2Locator);
        return this;
    }
    public StartAndManage CheckQuestion2Option3Text(){
        Question2Option3=ElementActions.getText(driver,Question2Option3Locator);
        return this;
    }

    public StartAndManage GetPreviousAnswerText(By Locator){
        PreviousAnswer=ElementActions.getText(driver,Locator);
        return this;
    }

    public StartAndManage ClickQuestion1Option1(){
        ElementActions.click(driver,RadioButton1);
        return this;
    }
    public StartAndManage ClickQuestion2Option3(){
        ElementActions.click(driver,RadioButton2);
        return this;
    }
    public StartAndManage ClickQuestion3Option1(){
        ElementActions.click(driver,RadioButton3);
        return this;
    }

    public StartAndManage PressNext(){
        ElementActions.click(driver,NextButton);
        return this;
    }
    public StartAndManage PressNextQuestion2(){
        ElementActions.click(driver,NextButtonQuestion2);
        return this;
    }
    public StartAndManage PressAccept(){
        ElementActions.click(driver,AcceptCookies);
        return this;
    }
}
