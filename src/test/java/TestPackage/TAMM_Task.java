package TestPackage;

import TAMM.Home;
import TAMM.Login;
import TAMM.StartAndManage;
import com.shaft.driver.DriverFactory;
import com.shaft.validation.Validations;
import io.qameta.allure.Description;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class TAMM_Task extends Setup{
    WebDriver driver;
    public String URL;
    public String className= this.getClass().getName().replace(this.getClass().getPackageName()+".","");
    @BeforeMethod
    public void setup(Method method) {
        driver= DriverFactory.getDriver();
        URL = testData.getTestData(method.getName()+".URL");
    }
    //Test Methods
    @Test
    @Description("Navigate")
    public void TestCase1(){
        new Home(driver).Navigate(URL);
        //Verify that home page loaded
        Validations.assertThat()
                .element(driver,Home.getSearchBox())
                .exists()
                .withCustomReportMessage("Page Loaded")
                .perform();

        new Home(driver).ClickOnSpecificCategory();
        //verify that start and manage page loaded
        Validations.assertThat()
                .element(driver, StartAndManage.getSearchResultCounter())
                .exists()
                .withCustomReportMessage("Start and Manage page Loaded")
                .perform();
        new StartAndManage(driver).StepByStepClick();

        //Check if question one exist..
        Validations.assertThat()
                .element(driver, StartAndManage.getQuestion1())
                .exists()
                .withCustomReportMessage("Question 1 Exist")
                .perform();

        //Check question one text
        new StartAndManage(driver).CheckQuestion1Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion1Text())
                .contains("1.What would you like to do?")
                .withCustomReportMessage("Question 1 Text is correct")
                .perform();


        //Check Question 1 options (6 options)

        new StartAndManage(driver).CheckQuestion1Option1Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion1Option1())
                .isEqualTo("Get a licence now")
                .withCustomReportMessage("Question 1 Option 1 Text is correct")
                .perform();
        System.out.println("ahmed magdy megz" + StartAndManage.getQuestion1Option1());


        new StartAndManage(driver).CheckQuestion1Option2Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion1Option2())
                .isEqualTo("Reserve Economic Name")
                .withCustomReportMessage("Question 1 Option 2 Text is correct")
                .perform();

        new StartAndManage(driver).CheckQuestion1Option3Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion1Option3())
                .isEqualTo("Renew an existing licence")
                .withCustomReportMessage("Question 1 Option 3 Text is correct")
                .perform();

        new StartAndManage(driver).CheckQuestion1Option4Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion1Option4())
                .isEqualTo("Amend my licence")
                .withCustomReportMessage("Question 1 Option 4 Text is correct")
                .perform();

        new StartAndManage(driver).CheckQuestion1Option5Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion1Option5())
                .isEqualTo("Obtain a commercial permit")
                .withCustomReportMessage("Question 1 Option 5 Text is correct")
                .perform();

        new StartAndManage(driver).CheckQuestion1Option6Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion1Option6())
                .isEqualTo("None of the above")
                .withCustomReportMessage("Question 1 Option 6 Text is correct")
                .perform();


        //Choose Option 1
        new StartAndManage(driver)
                .ClickQuestion1Option1()
                .PressAccept();
        //Press on Next Button and get the previous answer
        new StartAndManage(driver)
                .PressNext()
                .GetPreviousAnswerText(StartAndManage.getPreviousAnswerLocator());

        //Verify the previous Answer Text with the choosen one
        Validations.assertThat()
                .object(StartAndManage.getQuestion1Option1())
                .isEqualTo(StartAndManage.getPreviousAnswer())
                .withCustomReportMessage("Previous Answer is correct")
                .perform();


//Check Question 2
        //Check question one text
        new StartAndManage(driver).CheckQuestion2Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion2Text())
                .isEqualTo("2.Are you planning to open a new company?")
                .withCustomReportMessage("Question 2 Text is correct")
                .perform();


        //Check Question 2 options (3 options)

        new StartAndManage(driver).CheckQuestion2Option1Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion2Option1())
                .isEqualTo("Yes, I want to open a brand new business.")
                .withCustomReportMessage("Question 2 Option 1 Text is correct")
                .perform();
        System.out.println("ahmed magdy megz" + StartAndManage.getQuestion2Option1());


        new StartAndManage(driver).CheckQuestion2Option2Text();
        Validations.assertThat()
                .object(StartAndManage.getQuestion2Option2())
                .isEqualTo("Yes, I want to obtain a freelancer licence.")
                .withCustomReportMessage("Question 2 Option 2 Text is correct")
                .perform();
        new StartAndManage(driver).CheckQuestion2Option3Text();

        //Choose Option 3 in the question 2
        new StartAndManage(driver)
                .ClickQuestion2Option3();
        //Press on Next Button and get the previous answer
        new StartAndManage(driver)
                .PressNextQuestion2()
                .GetPreviousAnswerText(StartAndManage.getPreviousAnswerLocatorQuestion2());
        //Verify the previous Answer Text with the choosen one in question 2
        Validations.assertThat()
                .object(StartAndManage.getQuestion2Option3())
                .isEqualTo(StartAndManage.getPreviousAnswer())
                .withCustomReportMessage("Previous Answer for second question is correct")
                .perform();

        // Question 3 Choose the first option
        new StartAndManage(driver)
                .ClickQuestion3Option1()
                .PressNextQuestion2();

        Validations.assertThat()
                .element(driver, Login.getSignInButton())
                .exists()
                .withCustomReportMessage("Login Page Loaded")
                .perform();
    }

  }
