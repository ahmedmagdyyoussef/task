package TestPackage;

import com.shaft.gui.browser.BrowserActions;
import com.shaft.tools.io.JSONFileManager;
import io.qameta.allure.Description;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;


public class Setup {
    public WebDriver driver;
    public JSONFileManager testData;
    public String className= this.getClass().getName().replace(this.getClass().getPackageName()+".","");

    @AfterMethod
    @Description("Close the browser after the test")
    public void tearDown() {

        BrowserActions.closeCurrentWindow(driver);
        //DriverFactory.getDriver().close();
    }
    @BeforeClass()
    @Description("Setting cookie map file and test data file")
    public void beforeClass() {
        testData = new JSONFileManager("src/test/resources/TestDataFiles/" + className + ".json");
    }

}
