package TestPackage;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.shaft.api.RestActions;
import com.shaft.api.RestActions.RequestType;
import com.shaft.validation.Validations;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.json.JSONObject;
import org.testng.annotations.Test;

public class API {

    @Test
    public void GETFirstMethod() {
        RestActions apiObject = new RestActions("https://jsonplaceholder.typicode.com");
        Response users = apiObject.performRequest(RequestType.GET, 200, "/users");
        String Name=RestActions.getResponseJSONValue(users,"[6].name");
        //This is the name of record 6 , and you can assert on any thing else
        System.out.println(Name);
    }
    @Test
    public void AssertOnCode() {
        RestActions apiObject = new RestActions("https://jsonplaceholder.typicode.com");
        Response users = apiObject.performRequest(RequestType.GET, 200, "/users");
        int Code=RestActions.getResponseStatusCode(users);
        //This is the name of record 6 , and you can assert on any thing else
        Validations.assertThat()
                .object(Code)
                .isEqualTo("200")
                .withCustomReportMessage("The code is correct 200")
                .perform();

    }
    //check the first name and assert on it
    @Test
    public void GETSecondMethod() {
        RestActions apiObject = new RestActions("https://reqres.in");
        Response resp= apiObject.buildNewRequest("/api/users",RequestType.GET)
                .setContentType(ContentType.JSON)
                .performRequest();
        String FirstName=RestActions.getResponseJSONValue(resp,"data[4].first_name");
        System.out.println("ahmed magdy" + FirstName);
        Validations.assertThat()
                .object(FirstName)
                .isEqualTo("Charles")
                .withCustomReportMessage("Verified the first name")
                .perform();
    }
    //check the first name and assert on it
    @Test
    public void AssertOnCode2() {
        RestActions apiObject = new RestActions("https://reqres.in");
        Response resp= apiObject.buildNewRequest("/api/users",RequestType.GET)
                .setContentType(ContentType.JSON)
                .performRequest();
        int Code=RestActions.getResponseStatusCode(resp);

        System.out.println("Code is " + Code);
        Validations.assertThat()
                .object(Code)
                .contains("200")
                .withCustomReportMessage("Verified the Code with Magdy")
                .perform();
    }
    @Test
    public void GETWithParameters() {
        RestActions apiObject = new RestActions("https://reqres.in");
        Response resp= apiObject.buildNewRequest("/api/users/",RequestType.GET)
                .setContentType(ContentType.JSON)
                .setUrlArguments("id=2")
                .setTargetStatusCode(200)
                .performRequest();
        String FullResponse=RestActions.getResponseBody(resp);
        System.out.println("ahmed magdy" + FullResponse);

    }
    @Test
    public void DeleteWithParameters() {
        int UserID=1;  // i can get this number from another API by getJSONvalue method
        RestActions apiObject = new RestActions("https://reqres.in");
        Response resp= apiObject.buildNewRequest("/api/users/"+UserID,RequestType.DELETE)
                .setContentType(ContentType.JSON)
                .setTargetStatusCode(204)
                .performRequest();
        String allresp=RestActions.getResponseBody(resp);
        System.out.println("ahmed magdy" + allresp);

    }

    //Post new record
    @SneakyThrows
    @Test
    public void Post() {
        JSONObject body= new JSONObject();
            body.put("name", 123);
        body.put("job", "Khaled");

        RestActions apiObject = new RestActions("https://reqres.in");
        //We can use this method or the other one
     /*  Response resp= apiObject.buildNewRequest("/api/users",RequestType.POST)
                .setContentType(ContentType.JSON)
                .setTargetStatusCode(201)
                .setRequestBody(body)
                .performRequest();
*/
        Response users = apiObject.performRequest(RequestType.POST, 201, "/api/users",body, ContentType.JSON);
        String ResponseBody=RestActions.getResponseBody(users);
        int Code=RestActions.getResponseStatusCode(users);

        Validations.assertThat()
                .object(Code)
                .contains("201")
                .withCustomReportMessage("Verified the Code with Magdy")
                .perform();
        System.out.println("Response Body is :" + ResponseBody);
    }

    //This method to post new passenger another method
    @SneakyThrows
    @Test
    public void CreatPassengerUsingPost() {
      //  String jsonString = "{'name':'ahmed','trips':20 , 'airline' : 200}";
        //OR
        String jsonString = "{\"name\":\"ahmed magdy\", \"trips\":22, \"airline\":12}";
        JsonObject jsonObject = (JsonObject) JsonParser.parseString(jsonString);
        System.out.println("ahmed magdy youssef" + jsonObject.toString());
        RestActions apiObject = new RestActions("https://api.instantwebtools.net");
        Response users = apiObject.performRequest(RequestType.POST, 200, "/v1/passenger",jsonObject, ContentType.JSON);
        String Ahmed=RestActions.getResponseBody(users);
        System.out.println("ahmed magdy" + Ahmed);
    }


    @Test
    public void APITest(){
        RestActions apiObject = new RestActions("https://jsonplaceholder.typicode.com");
        Response users = apiObject.performRequest(RequestType.GET, 200, "/users");
        Validations.assertThat()
                .response(users)
                .isEqualToFileContent
                        ("C:\\ShaftProjectsIntellij\\src\\main\\resources\\TestData\\Return.json")
                .withCustomReportMessage("Check that getBookingResponse is equal to file Content")
                .perform();
       // Assertions.assertEquals("Leanne Graham", RestActions.getResponseBody(users), AssertionComparisonType.CONTAINS,
         //       AssertionType.POSITIVE);

//        RestActions.getResponseJSONValueAsList(users, "$").forEach(user -> {
           // if (RestActions.getResponseJSONValue(user, "name").equals("Leanne Graham")) {
             //   Assertions.assertEquals("Sincere@april.biz", RestActions.getResponseJSONValue(user, "email"),
               //         AssertionComparisonType.EQUALS, AssertionType.POSITIVE);
         //   }

  //      });
    }

}